function pack_library(folder_name, file_name)
%Combine the .m files in the folder "folder_name" and its sub-folders into a single .m file "file_name".
%Name conflicts are prevented and it is possible to call a sub-function in another .m file by writing
%"{name of the function}_extfunc_{file name without ".m"}", even it is not advised to do so.
%Your .m files must no contain the keyword "split@this_place".
%
%Inputs:
%   - folder_name: name of the folder where the .m files are
%   - file_name: name of the resulting .m file
%
%To access a function, named here F, you get the corresponding function handle
%by calling the function "file_name" with the name of F, which is a string, as input
%
%Author: Alcar Kerlomesy (https://gitlab.com/AlcarKerlomesy)
%License: Mozilla Public License 2.0
    
    list_mfiles = list_file_in_folder_rec(folder_name) ;
    list_main = cellfun(@(x) strtok(flip(strtok(flip(x), '/\')), '.'), list_mfiles, 'UniformOutput', false) ;
    [list_sub, sub_id] = get_subfunc_name(list_mfiles) ;
    [c_main_main, c_main_sub, c_sub_sub] = detect_name_conflict(list_main, list_sub) ;
    
    if ~isempty(c_main_main)
        disp('Name conflict: at least two .m files have the same name') ;
        return ;
    end
    
    %main routine
    library_id = fopen([file_name, '.m'], 'w') ;
    
    %main library function
    fprintf(library_id, 'function func_handle = %s(func_name)\n\n', file_name) ;
    fprintf(library_id, 'func_handle = eval([''@'', func_name]) ;\n\n') ;
    fprintf(library_id, 'end\n\n') ;
    
    %To indicate the function is somewhere else (only for external
    %subfunction), write {name of the function}_extfunc_{file name without ".m"}
    change_sub = identify_sub_change(list_mfiles, list_sub, sub_id, c_main_sub, c_sub_sub) ;
    
    for m1 = 1 : numel(list_mfiles)
        fwrite(library_id, prepare_text_merge(list_mfiles{m1}, list_main, m1, list_sub, sub_id, change_sub), 'char') ;
    end
    
    fclose(library_id) ;
    
    %For fun (remove unecessary spaces and newlines... make the resulting files smaller but uglier)
    library_id = fopen([file_name, '.m'], 'r') ;
    cur_text = fread(library_id, '*char').' ;
    fclose(library_id) ;
    
    cur_text = regexp(cur_text, '[\f\r\t\v]+', 'split') ;
    cur_text = strjoin(cur_text(~cellfun(@isempty, cur_text)), ' ') ;
    cur_text = regexp(cur_text, '\n+', 'split') ;
    cur_text = strjoin(cur_text(~cellfun(@isempty, cur_text)), newline) ;
    
    library_id = fopen([file_name, '.m'], 'w') ;
    fwrite(library_id, cur_text, 'char') ;
    fclose(library_id) ;

end

function list_mfiles = list_file_in_folder_rec(folder_name)

    info_dir = dir(folder_name) ;
    field_name = fieldnames(info_dir) ;
    dir_name = strcmp(field_name, 'name') ;
    isdir_flag = strcmp(field_name, 'isdir') ;
    info_dir = struct2cell(info_dir) ;
    
    info_dir = info_dir(:, ~cellfun(@(x) startsWith(x, '.'), info_dir(dir_name,:))) ;    
    list_dir = info_dir(dir_name, cellfun(@(x) x, info_dir(isdir_flag,:))) ;
    list_mfiles = info_dir(dir_name, cellfun(@(x) endsWith(x, '.m'), info_dir(dir_name,:))) ;
    list_mfiles = cellfun(@(x) [folder_name, '/', x], list_mfiles, 'UniformOutput', false) ;
    
    for m1 = 1 : numel(list_dir)
        list_mfiles = [list_mfiles, list_file_in_folder_rec([folder_name, '/', list_dir{m1}])] ;
    end

end

function [list_sub, sub_id] = get_subfunc_name(list_mfiles)

    list_sub = cell(1,0) ;
    sub_id = [] ;

    for m1 = 1 : length(list_mfiles)
       cur_file_id = fopen(list_mfiles{m1}, 'r') ;
       cur_sub = cellfun(@(x) x{1}, regexp(remove_comment_str(fread(cur_file_id, '*char').', false), '\<function\>(?:[^=\n]*=)?\s*(\w*)(?!\s*=)', 'tokens'), 'UniformOutput', false) ;
       list_sub = [list_sub, cur_sub(2:end)] ;
       sub_id = [sub_id, m1*ones(1,numel(cur_sub)-1)] ;
       fclose(cur_file_id) ;
    end

end

function [c_main_main, c_main_sub, c_sub_sub] = detect_name_conflict(list_main, list_sub)

    c_main_main = zeros(0,2) ;
    c_main_sub = zeros(0,2) ;
    c_sub_sub = zeros(0,2) ;

    for m1 = 1 : numel(list_main)
        cur_cmp = find(strcmp(list_main{m1}, list_main(m1+1:end))) ;
        if ~isempty(cur_cmp)
            c_main_main = [c_main_main ; [m1*ones(size(cur_cmp.')),cur_cmp.'+m1]] ;
        end
        
        cur_cmp = find(strcmp(list_main{m1}, list_sub)) ;
        if ~isempty(cur_cmp)
            c_main_sub = [c_main_sub ; [m1*ones(size(cur_cmp.')),cur_cmp.']] ;
        end
    end
    
    for m1 = 1 : numel(list_sub)
       cur_cmp = find(strcmp(list_sub{m1}, list_sub(m1+1:end))) ;
       if ~isempty(cur_cmp)
           c_sub_sub = [c_sub_sub ; [m1*ones(size(cur_cmp.')), cur_cmp.'+m1]] ;
       end
    end

end

function change_sub = identify_sub_change(list_mfiles, list_sub, sub_id, c_main_sub, c_sub_sub)

    change_sub = false(size(list_sub)) ;
    change_sub(c_main_sub(:,2)) = true ;
    change_sub(unique(c_sub_sub(:))) = true ;
    
    for m1 = 1 : numel(list_mfiles)
        cur_file_id = fopen(list_mfiles{m1}, 'r') ;
        
        cur_text = remove_comment_str(fread(cur_file_id, '*char').', false) ;
        
        for m2 = 1 : numel(list_sub)
           if ~change_sub(m2) && sub_id(m2) ~= m1 && ~isempty(regexp(cur_text, ['(?<!\.)(\<', list_sub{m2}, '\>)'], 'once'))
               change_sub(m2) = true ;
           end
        end
        
        fclose(cur_file_id) ;
    end

end

function text_to_merge = prepare_text_merge(list_mfile, list_main, file_id, list_sub, sub_id, change_sub)

    cur_file_id = fopen(list_mfile, 'r') ;

    [reduced_text, removed_str] = remove_comment_str(fread(cur_file_id, '*char').', true) ;
    
    for m1 = 1 : numel(list_sub)
       %Detect the "extfunc" extension and do the suitable change
       [cur_text, cur_tokens] = regexp(reduced_text, ['(?<!\.)(?:\<', list_sub{m1}, '_extfunc_)(\w*)'], 'split', 'tokens') ;
       ext_func = cell(1,numel(cur_tokens)) ;
       
       for m2 = 1 : numel(cur_tokens)
           temp_comp = strcmp(cur_tokens{m2}{1}, list_main) ;
           if any(temp_comp)
                if change_sub(m1)
                    ext_func{m2} = [list_sub{m1}, '_fileid', num2str(find(temp_comp))] ;
                else
                    ext_func{m2} = list_sub{m1} ;
                end
           else
               disp(['In the file ', list_mfile,', the function ', list_sub{m1}, '_extfunc_', cur_tokens{m2}{1}, ' is used. However, the file ', cur_tokens{m2}{1}, ' doesn''t exist!']) ;
               ext_func{m2} = [list_sub{m1}, '_unknownfile_', cur_tokens{m2}{1}] ;
           end
       end
       
       if ~isempty(cur_tokens)
          reduced_text = include_removed_part(cur_text, ext_func) ;
       end

       if change_sub(m1) && sub_id(m1) == file_id
          cur_text =  regexp(reduced_text, ['(?<!\.)(\<', list_sub{m1}, '\>)'], 'split') ;
          reduced_text = include_removed_part(cur_text, repmat({[list_sub{m1}, '_fileid', num2str(file_id)]} , 1, numel(cur_text)-1)) ;
       end
    end
    
    text_to_merge = [include_removed_part(regexp(reduced_text, ' split@this_place ', 'split'), removed_str), newline] ;
    
    fclose(cur_file_id) ;

end

function [reduced_text, removed_str] = remove_comment_str(input_text, add_marker)
    
    %Remove strings in code and in comment. It takes care of the transpose
    %sign
    [reduced_text, removed_str] = regexp(input_text, '((?<!(\w|[.)]))''|")[^\n]*?\1', 'split', 'match') ;
    
    if (add_marker)
        reduced_text = strjoin(reduced_text, ' split@this_place ') ;
    else
        reduced_text = strjoin(reduced_text) ;
    end
    
    %Remove comments
    [reduced_text, comment_text] = regexp(reduced_text, '%.*?(\n|$)', 'split', 'match') ;
    
    %Check how many strings are in comments and remove them from
    %removed_str
    if (add_marker)
        num_in = cellfun(@length, regexp(reduced_text, ' split@this_place ')) ;
        num_comment = cellfun(@length, regexp(comment_text, ' split@this_place ')) ;
    
        num_tot = ones(1,length(num_in)+length(num_comment)) ;
        num_tot(1:2:end) = num_in ;
        num_tot(2:2:end) = num_comment ;
    
        removed_str = removed_str(repelem([repmat([true, false],[1,length(num_comment)]),true], num_tot)) ;
    end
    
    reduced_text = strjoin(reduced_text, '\n') ; 
    reduced_text = strjoin(regexp(reduced_text, '\.\.\.[^\n]*\n', 'split'), ' ') ;

end

function merged_text = include_removed_part(reduced_text, removed_part)

    merged_text = cell(1, numel(reduced_text) + numel(removed_part)) ;
    merged_text(1:2:end) = reduced_text ;
    merged_text(2:2:end) = removed_part ;
    merged_text = cat(2,merged_text{:}) ;

end